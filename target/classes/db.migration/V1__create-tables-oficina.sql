
CREATE TABLE mecanico(
                         id serial unique primary key,
                         nome varchar(100),
                         cpf varchar(14),
                         login varchar(100),
                         senha varchar(100),
                         permissao varchar(20),
                         salario numeric(10,5)

);

CREATE TABLE cliente(
                        id serial unique primary key,
                        nome varchar(100),
                        cpf varchar(14),
                        login varchar(100),
                        senha varchar(100),
                        permissao varchar(20),
                        telefone varchar(20)

);


CREATE TABLE produto (
                         id serial unique primary key,
                         nome varchar(30)
);

CREATE TABLE ordem_servico(
                              id serial unique primary key,
                              marca varchar(50),
                              modelo varchar(50),
                              tipo varchar(50),
                              problema varchar(100),
                              status varchar(20),
                              data_entrada varchar(10),
                              data_saida varchar(10),
                              total numeric(10,5),
                              ativo boolean,
                              id_mecanico int,
                              id_cliente int,
                              FOREIGN KEY (id_mecanico) references mecanico(id),
                              FOREIGN KEY (id_cliente) references cliente(id)
);

CREATE TABLE custo (
                       id serial unique primary key,
                       valor numeric(10,5),
                       id_ordemservico int,
                       id_produto int,
                       FOREIGN KEY (id_produto) REFERENCES produto (id),
                       FOREIGN KEY (id_ordemservico) REFERENCES ordem_servico (id)
);
