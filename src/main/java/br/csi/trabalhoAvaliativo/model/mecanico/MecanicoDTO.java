package br.csi.trabalhoAvaliativo.model.mecanico;

public interface MecanicoDTO {
    Long getIdMecanico();
    String getNome();
    String getCpf();
    String getSalario();}
