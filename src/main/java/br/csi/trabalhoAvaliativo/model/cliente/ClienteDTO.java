package br.csi.trabalhoAvaliativo.model.cliente;

public interface ClienteDTO {

    Long getIdCliente();
    String getNome();
    String getCpf();
    String getTelefone();
}
