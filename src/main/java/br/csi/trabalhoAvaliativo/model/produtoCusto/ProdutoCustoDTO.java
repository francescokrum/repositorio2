package br.csi.trabalhoAvaliativo.model.produtoCusto;

public interface ProdutoCustoDTO {
    Long getId();
    String getNome();
}
